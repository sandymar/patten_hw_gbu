﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBU_PatternLessons
{
    interface ILessons
    {
        /// <summary>
        /// Общий метод, запускающий выполнение ДЗ
        /// </summary>
        void Execute();
    }
}

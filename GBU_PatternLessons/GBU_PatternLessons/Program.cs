﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBU_PatternLessons
{
    class Program
    {
        static void ShowHead(int lessonNum)
        {
            string textHead = $"Александра Марциус, ДЗ по уроку №{lessonNum}";
            Console.WriteLine(textHead);
        }

        static void Main(string[] args)
        {
            ShowHead(1);
            ILessons lesson = new Lesson1();
            lesson.Execute();
        }
    }
}

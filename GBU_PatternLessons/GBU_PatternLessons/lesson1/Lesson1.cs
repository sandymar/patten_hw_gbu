﻿using GBU_PatternLessons.lesson1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBU_PatternLessons
{
    class Lesson1 : ILessons
    {
        #region Задание №1. Рефакторинг с использованием базового класса
        /// <summary>
        /// Базовый класс для ДЗ.
        /// </summary>
        public abstract class EntityBase
        {
            public long Id { get; private set; }
            public EntityBase()
            {
                Id = CalculateId();
            }

            /// <summary>
            /// Показать идентификатор в консоли
            /// </summary>
            public void ShowIt()
            {
                Console.WriteLine($"Текущий Id: {Id}");
            }
            /// <summary>
            /// Рассчитывает новый идентификатор
            /// Виртуальный, т.к. возможно в одном из потомков потребуется изменить расчет ID
            /// </summary>
            /// <returns>Новый идентификатор</returns>
            protected virtual long CalculateId()
            {
                long id = DateTime.Now.Ticks;
                return id;
            }
        }

        public class Customer : EntityBase
        {
            public string Description { get; set; }
            public Customer() { }
        }

        public class Store : EntityBase
        {
            public Store() { }
        }
        #endregion

        #region Задание 2: Повторяющиеся фрагменты кода с использованием Func
        public static readonly string Address = Constants.Address;
        public static readonly string Format = Constants.Format;
        private static List<Func<string>> actionsList = new List<Func<string>>
        {
            DummyFunc,
            DummyFuncAgain,
            DummyFuncMore
        };

        private static string DummyFunc()
        {
            return WriteToConsole("Петя", "школьный друг", 30);
        }
        private static string DummyFuncAgain()
        {
            return WriteToConsole("Вася", "сосед", 54);
        }
        private static string DummyFuncMore()
        {
            return WriteToConsole("Николай", "сын", 4);
        }

        private static string WriteToConsole(string name, string description,
        int age)
        {
            return String.Format(Format, name, description, Address, age);
        }
        #endregion

        public void Execute()
        {
            //Задание №1: Повторяющаяся логика
            Console.WriteLine("\nЗадание №1. Рефакторинг с использованием EntityBase \n");
            Customer c = new Customer();
            Store s = new Store();
            c.ShowIt();
            s.ShowIt();

            //Задача №2: Повторяющиеся фрагменты кода с использованием Func
            Console.WriteLine("\nЗадание №2. Повторяющиеся фрагменты кода с использованием Func \n");
            foreach (Func<string> a in actionsList) Console.WriteLine(a.Invoke());
            Console.ReadLine();

        }        
    }
}
